<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distance extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'distance', 'unit'];

    /**
     * Get the distance that belongs to the races.
     */
    public function races()
    {
        return $this->belongsToMany('App\Race');
    }

    /**
     * Get the distance that belongs to the races.
     */
    public function contestants()
    {
        return $this->belongsToMany('App\Contestant');
    }
}


