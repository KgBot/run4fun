<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'place', 'date', 'time', 'status'];

    /**
     * Get the user that owns races.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
//            ->where('user.id', '=', 'race_user.user_id');
    }

    /**
     * Get the race that owns contestants.
     */
    public function contestants()
    {
        return $this->belongsToMany('App\Contestant');
//            ->where('contestant.id', '=', 'contestant_race.contestant_id');
    }

    /**
     * Get the race that owns distances.
     */
    public function distances()
    {
        return $this->belongsToMany('App\Distance');
    }
}
