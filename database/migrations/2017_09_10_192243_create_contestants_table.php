<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contestants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bib_number');
            $table->string('name');
            $table->string('gender'); # male or female
            $table->string('dob');
            $table->integer('age');
            $table->string('phone');
            $table->string('email');
            $table->string('country');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('contestants');
        Schema::enableForeignKeyConstraints();
    }
}
