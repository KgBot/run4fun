<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')
    ->name('home');

//# SHOW ALL DISTANCES THAT BELONG TO A CONTESTANT
//Route::get('/distance/distancecontestant/{id}', 'DistanceController@showDistanceContestant')
//    ->name('DistanceContestant');
//
//# SHOW ONE CONTESTANT THAT BELONGS TO A RACE
//Route::get('/race/contestant/{id}', 'RaceController@showContestant')
//    ->name('showContestant');
//
//# SHOW ALL RACES THAT BELONG TO A USER
//Route::get('/user/{id}/races/', 'UserController@showRaces')
//    ->name('showRaces');
//
//# SHOW ONE DISTANCE THAT BELONGS TO A CONTESTANT
//Route::get('contestant/distances/{id}', 'ContestantController@showDistances')
//    ->name('showDistances');
//
//# SHOW ALL CONTESTANTS THAT BELONG TO A RACE
//Route::get('race/{id}/racescontestants', 'RaceController@showRacesContestants')
//    ->name('showRacesContestants');
